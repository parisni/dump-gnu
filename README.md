GOAL
----

This build a compressed tarball and send it over ssh. The remote server
optionally crypts the tarball.

The program read the folders from a configuration file and allow to look only
for new files changed several days ago.

USAGE
-----

```bash
Dump full or incremetal list of folders over ssh, compress them and optionally crypt them
Usage: ./dumpgnu [-d|--day-before <arg>] [-h|--help] <file-pass> <folder-list> <remote-host> <remote-file>
	<file-pass>: a file containing the password to encrypt
	<folder-list>: specify the folder file path. A txt file with one folder per line
	<remote-host>: specify the remote host name for ssh access
	<remote-file>: specify the remote file (the date and type will be added)
	-d, --day-before: specify the number of day to look for new files (default: '')
	-h, --help: Prints help
```

EXAMPLE
-------

```
# gzip and encode the folders and send them on the remote host with prefix /backups/dump.yyyymmdd_hhMMss.tar.gz
dumpgnu password-file /tmp/fileToMove.txt user@host /backups/dump

# only send the file newer than 20 days
dumpgnu password-file /tmp/fileToMove.txt user@host /backups/dump -d 20
```


CRYPT
-----

How to decrypt and decompress:

1. decrypt the file and untar: `openssl enc -d -aes256 -pbkdf2 -in dump.tar.gz | tar xz -C <target-folder>`

